/*!
	Genealogy
	v1.0
	License: MIT
*/


/*
TODO update by using a js loader type requiredJs
var Papa = require("../papaparse.js");
var fs = require('fs');
*/

(function(root, factory)
{
	if (typeof define === 'function' && define.amd)
	{
		// AMD. Register as an anonymous module.
		define([], factory);
	}
	else if (typeof module === 'object' && typeof exports !== 'undefined')
	{
		// Node. Does not work with strict CommonJS, but
		// only CommonJS-like environments that support module.exports,
		// like Node.
		module.exports = factory();
	}
	else
	{
		// Browser globals (root is window)
		root.genealogy = factory();
	}
}(this, function()
{
	'use strict';

	var global = (function () {
		// alternative method, similar to `Function('return this')()`
		// but without using `eval` (which is disabled when
		// using Content Security Policy).

		if (typeof self !== 'undefined') { return self; }
		if (typeof window !== 'undefined') { return window; }
		if (typeof global !== 'undefined') { return global; }

		// When running tests none of the above have been defined
		return {};
	})();


	var genealogy = {};
	genealogy.repository=new PersonRepository();


	function loadCSV(_input,_onComplete)
	{
		Papa.parse(_input, {
			//header : true,
			skipEmptyLines : true,
			encoding : 'ISO-8895-1',
			complete: function(results) {
				CSVtoBean(results.data);
				_onComplete();
			}
		});
	}


	function CSVtoBean(_csvData)
	{
		_csvData.forEach(function(csvLine) {
    		genealogy.repository.add(CSVLineToPerson(csvLine));
		});
	}

	function CSVLineToPerson(_CSVLine){
		var _sexe=(typeof _CSVLine[4] !== 'undefined')?(_CSVLine[4].startsWith("M") ? Person._SEX_MALE : (_CSVLine[4].startsWith("F") ? Person._SEX_FEMALE : null)):null;
		var _birthCertificate = new BirthCertificate(_CSVLine[10],_CSVLine[11],_CSVLine[12],null,null,_CSVLine[13]);
		var _deathCertificate = new DeathCertificate(_CSVLine[18],_CSVLine[19],null,null,null,_CSVLine[20]);
		var _weddingCertificates = [new WeddingCertificate(_CSVLine[21],_CSVLine[22],[_CSVLine[0],_CSVLine[24]],_CSVLine[21],_CSVLine[22],_CSVLine[23])];
		var _nominationCertificates = new NominationCertificate(_CSVLine[14],_CSVLine[16],_CSVLine[15],null,_CSVLine[17]);
		
		var person = new Person(_CSVLine[0],_CSVLine[2],_CSVLine[1],null,_CSVLine[3],_birthCertificate,_deathCertificate,_weddingCertificates,_CSVLine[6],_CSVLine[5],_sexe,_nominationCertificates);
		
		return person;
	}

	function onCSVChange(){
		genealogy.loadCSV(document.getElementById('csvFile').files[0],function(){
		genealogy.updateFamilyOption(document.getElementById('familySelect'));
		});
		
	}

	function updateFamilyOption(_selectField){
		genealogy.repository.persons.forEach(function(_person){
			var opt = new Option();
		    opt.value = _person.id;
		    opt.innerHTML = _person.id+ " : "+ _person.firstName + " "+ _person.lastName;
		    _selectField.add(opt);
		});
	}


	function drawFamilly(_person){
		var vertex1=drawPerson(_person,genealogy.graph,50,50,genealogy.template.names.person);
		if (_person.weddingCertificates){
		var vertex2=drawWedding(_person.weddingCertificates[0],genealogy.graph,500,50,genealogy.template.names.wedding);
		var edge=drawEdge(vertex1,vertex2);
		var vertex3=drawPerson(_person.weddingCertificates[0].,genealogy.graph,50,50,genealogy.template.names.person);
		}
	}

	function drawEdge(_vertex1,_vertex2){
		var _graph= genealogy.graph;
		var options ="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;endArrow=none;endFill=0;jettySize=auto;orthogonalLoop=1;"
		_graph.insertEdge(_graph.parent,null,'',_vertex1,_vertex2,options);
	}

	function drawPerson(_person,_graph,_posx,_posy,_template){
		var options = 'html=1;';
		if (_person.sexe == Person._SEX_FEMALE){
			options += 'rounded=1;'
		}
		var htmlLabel=applyTemplate(genealogy.template[_template],_person);

		var vertex= _graph.insertVertex(_graph.parent, null, htmlLabel, _posx, _posy, 240, 240,options);
		return vertex;
	}

	function drawWedding(_weddingCertificates,_graph,_posx,_posy,_template){
		var options = 'html=1;';
		var htmlLabel=applyTemplate(genealogy.template[_template],_weddingCertificates);
		var vertex= _graph.insertVertex(_graph.parent, null, htmlLabel, _posx, _posy, 150, 240,options);
		return vertex;
	}

	function loadDefaultTemplate(){
		loadTemplateFile('/src/template/person.html',genealogy.template.names.person);
		loadTemplateFile('/src/template/wedding.html',genealogy.template.names.wedding);
	}

	function loadTemplateFile(_templateURL,_templateName){
		var client = new XMLHttpRequest();
			client.open('GET', _templateURL);
			client.onloadend = function() {
		 		genealogy.template[_templateName]=client.responseText;
			}
		client.send();
	}

	function applyTemplate(_template,_data){
		var template = Handlebars.compile(_template);
		return template(_data);
	}
	
function printGraph(_graph){

var preview = new mxPrintPreview(_graph);
preview.open();
}

	function exportGraph(format, _graph){
					var bg = '#ffffff';
					var scale = 1;
					var b = 1;
					
					var imgExport = new mxImageExport();
					var bounds = _graph.getGraphBounds();
					var vs = _graph.view.scale;
					
					// New image export
					var xmlDoc = mxUtils.createXmlDocument();
					var root = xmlDoc.createElement('output');
					xmlDoc.appendChild(root);
					
					// Renders graph. Offset will be multiplied with state's scale when painting state.
					var xmlCanvas = new mxXmlCanvas2D(root);
					xmlCanvas.translate(Math.floor((b / scale - bounds.x) / vs), Math.floor((b / scale - bounds.y) / vs));
					xmlCanvas.scale(scale / vs);
					
					imgExport.drawState(_graph.getView().getState(_graph.model.root), xmlCanvas);
					// Puts request data together
					var w = Math.ceil(bounds.width * scale / vs + 2 * b);
					var h = Math.ceil(bounds.height * scale / vs + 2 * b);
					
					var xml = mxUtils.getXml(root);
						
					if (bg != null)
					{
						bg = '&bg=' + bg;
					}
					
					new mxXmlRequest('/Export', 'filename=export.' + format + '&format=' + format +
	        			bg + '&w=' + w + '&h=' + h + '&xml=' + encodeURIComponent(xml)).
	        			simulate(document, '_blank');
	}


	function initGraph(/*_container*/){
		var _container=document.getElementById('graphContainer');

		// Checks if the browser is supported
			if (!mxClient.isBrowserSupported())
			{
				// Displays an error message if the browser is not supported.
				mxUtils.error('Browser is not supported!', 200, false);
			}
			else
			{
				// Disables the built-in context menu
				mxEvent.disableContextMenu(_container);
				
				// Creates the graph inside the given container
				genealogy.graph = new mxGraph(_container);

				
				// Enables HTML labels as wrapping is only available for those
				genealogy.graph.setHtmlLabels(true);

				// Enables rubberband selection
				new mxRubberband(genealogy.graph);
				
				genealogy.graph.parent = genealogy.graph.getDefaultParent();
			}
	}



function clearGraph(/*_container*/){

	var graph=genealogy.graph;

	graph.removeCells(graph.getChildCells(graph.getDefaultParent()));
}


genealogy.template={};
genealogy.template.names={};
genealogy.template.names.person='person';
genealogy.template.names.wedding='wedding';

genealogy.CSVLineToPerson=CSVLineToPerson;
genealogy.loadCSV=loadCSV;
genealogy.onCSVChange=onCSVChange;
genealogy.updateFamilyOption=updateFamilyOption;
genealogy.initGraph=initGraph;
genealogy.applyTemplate=applyTemplate;
genealogy.loadTemplateFile=loadTemplateFile;
genealogy.drawPerson=drawPerson;
genealogy.drawFamilly=drawFamilly;
genealogy.exportGraph=exportGraph;
genealogy.printGraph=printGraph;
genealogy.clearGraph=clearGraph;

loadDefaultTemplate();

return genealogy;

}));

