function Person(){
	this.id= null;
	this.firstName= null;
	this.lastName= null;
	this.middleNames= [];
	this.surname=null;
	this.birthCertificate;
	this.deathCertificate;
	this.weddingCertificates=[];
	this.mother;
	this.father;
	this.sexe;
}

function Person(_id,_firstName,_lastName,_middleNames,_surname,_birthCertificate,_deathCertificate,_weddingCertificates,_motherId,_fatherId,_sexe,_nominationCertificate){
	this.id=_id;
	this.firstName= _firstName;
	this.lastName= _lastName;
	this.middleNames= _middleNames || [];
	this.surname=_surname;
	this.birthCertificate=_birthCertificate;
	this.deathCertificate=_deathCertificate;
	this.weddingCertificates=_weddingCertificates || [];
	this.nominationCertificates=_nominationCertificate;	
	this.motherId=_motherId;
	this.fatherId=_fatherId;
	this.sexe=_sexe;
}


Person._SEX_MALE="MALE";
Person._SEX_FEMALE="FEMALE";

function Certificate(_certificateType,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness){
	this.type=_certificateType;
	this.certificateDate=_certificateDate;
	this.certificatePlace=_certificatePlace;
	this.certificateNo=_certificateNo;
	this.witness=_certificateWitness||[];
}

Certificate._CERTIFICATETYPE_BIRTH="BIRTH";
Certificate._CERTIFICATETYPE_DEATH="DEATH";
Certificate._CERTIFICATETYPE_WEDDING="WEDDING";
Certificate._CERTIFICATETYPE_NOMINATION="NOMINATION";



function BirthCertificate(_birthDate,_birthPlace,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness){
	Certificate.call(this,Certificate._CERTIFICATETYPE_BIRTH,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness);
	this.birthDate=_birthDate;
	this.birthPlace=_birthPlace;
    

}
BirthCertificate.prototype = Object.create(Certificate.prototype);
BirthCertificate.prototype.constructor = BirthCertificate;


function DeathCertificate(_deathDate,_deathPlace,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness){
	Certificate.call(this,Certificate._CERTIFICATETYPE_DEATH,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness);
	this.deathDate=_deathDate;
	this.deathPlace=_deathPlace;
}
DeathCertificate.prototype = Object.create(Certificate.prototype);
DeathCertificate.prototype.constructor = DeathCertificate;

function WeddingCertificate(_weddingDate,_weddingPlace,_partners,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness){
	Certificate.call(this,Certificate._CERTIFICATETYPE_WEDDING,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness);
	this.weddingDate=_weddingDate;
	this.weddingPlace=_weddingPlace;
	this.partners=_partners||[];
}
WeddingCertificate.prototype = Object.create(Certificate.prototype);
WeddingCertificate.prototype.constructor = WeddingCertificate;


function NominationCertificate(_nominationDate,_nominationPlace,_nominationHabitation,_nominationAge,_matricule,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness){
	Certificate.call(this,Certificate._CERTIFICATETYPE_NOMINATION,_certificateDate,_certificatePlace,_certificateNo,_certificateWitness);
	this.type=Certificate._CERTIFICATETYPE_NOMINATION;
	this.matriculeNo=_matricule;
	this.nominateAge=_nominationAge;
	this.nominationDate=_nominationDate;
	this.nominationPlace=_nominationPlace;
	this.nominationHabitation=_nominationHabitation;
}
NominationCertificate.prototype = Object.create(Certificate.prototype);
NominationCertificate.prototype.constructor = NominationCertificate;


function PersonRepository(){
	this.persons=[];


	this.add=function(_person){
		this.persons.push(_person);
	}

	this.getPerson=function(_personID){
		return this.persons.filter(function(_person){
			return _person.id ==_personID;
		})[0];
	}

	this.getPartner=function(_personID){
		var me=this.getPerson(_personID);
		var latestDate = new Date(Math.max.apply(null, me.weddingCertificates.map(function(e) {return new Date(e.weddingDate);})));
		var partner = me.weddingCertificates.filter(function(_wedding){
			return new Date(_wedding.weddingDate) == latestDate;
		})[0];
		this.getPerson(partner);
	}

	this.getSiblings= function(_personID){
		var siblings=[];
		var siblingsId=[];
		var referent= this.getPerson(_personID);
		siblings = this.persons.filter(function(_person){
			return (_person.id!= _personID)
			&& ((_person.fatherId==referent.fatherId && _person.fatherId!="")
			 ||(_person.motherId==referent.motherId && _person.motherId!=""));
		});

		
		return siblings;
	}

	this.getChilds= function(_personID){
		var siblings=[];
		var referent= this.getPerson(_personID);
		childs = this.persons.filter(function(_person){
			return (_person.fatherId==referent.id && _person.fatherId!="")
			 ||(_person.motherId==referent.id && _person.motherId!="");
		});

		return childs;
	}

	this.getFamillyChilds= function(_parentsId){
		var siblings=[];
		
		childs = this.persons.filter(function(_person){
			return (_person.fatherId!="" && _parentsId.includes(_person.fatherId))
			 ||(_person.motherId!="" && _parentsId.includes(_person.motherId));
		});
		return childs;
	}


}

function SosaTree(){

}


function famillyTree(_person){

}