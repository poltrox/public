#Genealogy Client : 

A Browser client to import genealogy data from a csv sources, display them as diagram and enable print functionnality.
This project is still in Work in progress.
The goal of the version 1.0 is to be able to load a CSV file containing the Genealogy data and to be able to play with this data to generate :
* Details familly tree (Direct relative to the personne selected (Husband/Wife + Childs ) Draw the details familly tree for the person selected 
* Summary Familly Tree ( Following the sosa notation ) Draw the full Ascendant Familly tree for the person selected 
The final goal is to be able to print these 2 trees diagram.

Why working on this ? My wife tried to create trace her familly history and I saw her draw manually her trees. After each new information grabbed this ask work effort to report the new information in multiple vue.


This project currenly only aim to be run locally.

##Install
To install the local server : npm install   
To start the server : npm start

## Client Folder
The client Folder contain the webapplication to import and draw the Familly Graph

##Excelcsv Folder
This folder contain Some CSV sample to import data into the application. This give the default import format. 

